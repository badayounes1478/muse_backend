const multer = require('multer')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './Uploads')
    },
    filename: function (req, file, cb) {
        let fileName = file.originalname.replace(/ /g, '')
        cb(null, Date.now() + fileName.replace(/[^A-Za-z0-9.]/g, ''))
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/svg+xml') {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

Upload = multer({ storage: storage, fileFilter: fileFilter }).array('media', 10)

module.exports = Upload