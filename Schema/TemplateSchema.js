const mongoose = require('mongoose')
const schema = mongoose.Schema

const template = new schema({
    organizationId: String,
    userId: String,
    userName: String,
    template: String,
    canvasWidth: Number,
    canvasHeight: Number,
    canvasMultiply: Number,
    projectName: String,
    templateType: String,
    adminTemplateId: String,
    isDeleted: Boolean,
    isFavorite: Boolean
}, { timestamps: true })

module.exports = mongoose.model('template', template)