const mongoose = require('mongoose')
const schema = mongoose.Schema

const adminTemplate = new schema({
    userId: String,
    template: String,
    canvasWidth: Number,
    canvasHeight: Number,
    canvasMultiply: Number,
    projectName: String,
    description: String,
    colors: [{
        type: String,
        trim: true,
        lowercase: true
    }],
    category: [{
        type: String,
        trim: true,
        lowercase: true
    }],
    tags: [{
        type: String,
        trim: true,
        lowercase: true
    }],
    region: [{
        type: String,
        trim: true,
        lowercase: true
    }],
    templateType: String,
    aspectRatio: String,
    language: String,
    categoryDate: String,
    component: Boolean,
    verified: {
        type: Boolean,
        default: false
    },
    prime: {
        type: Boolean,
        default: false
    }
}, { timestamps: true })

adminTemplate.index({ "$**": "text" })


module.exports = mongoose.model('adminTemplate', adminTemplate)
