const mongoose = require('mongoose')
const schema = mongoose.Schema

const userUploads = new schema({
    organizationId: String,
    userId: String,
    uploadedBy: String,
    url: String,
    fileName: String,
    fileSize: Number,
    type: String,
    mimetype: String,
    userFileName: String,
    isDeleted: Boolean,
    isFavorite: Boolean,
}, { timestamps: true })

module.exports = mongoose.model('UserUploads', userUploads)