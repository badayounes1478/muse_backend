const mongoose = require('mongoose')
const schema = mongoose.Schema

const templateImages = new schema({
    userId: String,
    originalURL: String,
    s3URL: String,
    fileName: String,
})

module.exports = mongoose.model('templateImages', templateImages)