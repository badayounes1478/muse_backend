const mongoose = require('mongoose')
const schema = mongoose.Schema

const category = new schema({
    category: {
        type: String,
        lowercase: true,
        trim: true,
        unique: true
    },
    rank: Number
})

module.exports = mongoose.model('category', category)