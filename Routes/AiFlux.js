const express = require("express")
const router = express.Router()
const axios = require("axios")
const CheckAuth = require("../Middleware/CheckAuth")
const BFL_API_KEY = '0c572f79-394a-4922-8816-369c8415531d'
const BASE_URL = "https://api.bfl.ml/v1"
const AiImages = require('../Schema/AiImages');
const { v4: uuidv4 } = require('uuid');
const sharp = require('sharp')
const { checkAiImageCreditsAreThere, removeImageAiCredits } = require("../Functions/AiImageCredits")
const { addAiImageToBucket } = require("../Functions/S3")

const pollResult = async (requestId) => {
    try {
        let attempt = 0;
        const maxAttempts = 10;

        while (attempt < maxAttempts) {
            await new Promise(resolve => setTimeout(resolve, 1500)); // Sleep for 1 seconds

            // GET request to check the result status
            const response = await axios.get(`${BASE_URL}/get_result?id=${requestId}`, {
                headers: {
                    'accept': 'application/json',
                    'x-key': BFL_API_KEY
                }
            });

            const status = response.data.status;
            if (status === 'Ready') {
                const resultSample = response.data.result.sample;
                return resultSample; // Return the result when ready
            }
        }

        return null
    } catch (error) {
        console.log(error)
        return null
    }
};


const getImageMetadataFromUrl = async (url) => {
    try {
        // Fetch the image from the URL
        const response = await axios({
            url,
            responseType: 'arraybuffer',
        });

        // Convert the response data to a buffer
        let buffer = Buffer.from(response.data);

        buffer = await sharp(buffer)
            .resize({ height: 1024 }) // Resize to a specific height
            .jpeg({ quality: 80 }) // Adjust quality to reduce size
            .toBuffer();

        // Use sharp to extract metadata
        const metadata = await sharp(buffer).metadata();

        return {
            metadata,
            buffer
        };
    } catch (error) {
        console.log(error)
        return {
            metadata: null,
            buffer: null
        };
    }
}

router.post("/generate-image", CheckAuth, async (req, res) => {
    try {
        const { prompt, model = "flux-pro-1.1-ultra", organizationUserId, userData, organizationId } = req.body;
        const isCreditsThere = await checkAiImageCreditsAreThere(req)
        if (!isCreditsThere) return res.status(429).json({ message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account." })

        const data = {
            prompt: prompt,
            safety_tolerance: 2,
            prompt_upsampling: true,
        };

        const postResponse = await axios.post(`${BASE_URL}/${model}`, data, {
            headers: {
                'accept': 'application/json',
                'x-key': BFL_API_KEY,
                'Content-Type': 'application/json'
            }
        })

        const requestId = postResponse.data.id;
        const result = await pollResult(requestId);
        if (!result) return res.status(500).json({ error: "Failed to generate image" })

        const fileName = "ai" + uuidv4();

        const { metadata, buffer } = await getImageMetadataFromUrl(result)
        if (!metadata && !buffer) return res.status(500).json({ error: "Failed to generate image" })

        const { size, width, height, format } = metadata
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        // Consolidated image info to store in the database
        const info = {
            organizationUserId: organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            prompt,
            url,
            model: model,
            dimensions: { width, height },
            format,
            fileName,
            fileSize: size,
        };

        const aiImageData = await AiImages.create(info);

        const infoData = {
            _id: aiImageData._id,
            url: url,
            fileName,
        }

        removeImageAiCredits(req, model)
        res.send([infoData])
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: error?.message || "Internal Server Error"
        });
    }
})


router.post("/inpaint", CheckAuth, async (req, res) => {
    try {
        const { originalImage, maskImage, prompt, organizationUserId, userData, organizationId } = req.body;

        const isCreditsThere = await checkAiImageCreditsAreThere(req)
        if (!isCreditsThere) return res.status(429).json({ message: "Oops! It looks like you've reached your image creation limit. To keep creating amazing images, you can either upgrade to a Pro plan or add more credits to your account." })

        const data = {
            image: originalImage.split(",")[1],
            mask: maskImage.split(",")[1],
            prompt: prompt,
            steps: 50,
            prompt_upsampling: false,
            guidance: 60,
            output_format: "jpeg",
            safety_tolerance: 2,
        };

        const postResponse = await axios.post(`${BASE_URL}/flux-pro-1.0-fill`, data, {
            headers: {
                'x-key': BFL_API_KEY,
                'Content-Type': 'application/json'
            }
        })

        const requestId = postResponse.data.id;
        const result = await pollResult(requestId);
        if (!result) return res.status(500).json({ error: "Failed to generate image" })

        const fileName = "ai" + uuidv4();

        const { metadata, buffer } = await getImageMetadataFromUrl(result)
        if (!metadata && !buffer) return res.status(500).json({ error: "Failed to generate image" })

        const { size, width, height, format } = metadata
        const url = await addAiImageToBucket(buffer, fileName, `image/${format}`)

        // Consolidated image info to store in the database
        const info = {
            organizationUserId: organizationUserId,
            organizationId: organizationId,
            createdBy: userData.userName,
            prompt,
            url,
            model: "inpaint",
            dimensions: { width, height },
            format,
            fileName,
            fileSize: size,
        };

        await AiImages.create(info);

        removeImageAiCredits(req, "inpaint")
        res.send(url)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: error?.message || "Internal Server Error"
        });
    }
})

module.exports = router;