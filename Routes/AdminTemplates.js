const express = require('express')
const router = express.Router()
const adminTemplates = require('../Schema/AdminTemplateSchema')
const templateImages = require('../Schema/TemplateImages')
const { downloadTheImageFromUrlAndStoreToS3, addBase64DataToS3Bucket, deleteOldFile } = require('../Functions/S3')
const checkAuth = require('../Middleware/CheckAuth')

// new category
const componentCategory = require('../Schema/ComponentCategory')
const landscapeCategory = require('../Schema/LandscapeCategory')
const storyCategory = require('../Schema/StroyCategory')
const instagramCategory = require('../Schema/InstagramCategory')
const instagramVerticalCategory = require('../Schema/InstagramVerticalCategory')
const presentationCategory = require('../Schema/PresentationCategory')
const facebookCategory = require('../Schema/FacebookCategory')
const youtubeThumbnailCategory = require('../Schema/YoutubeThumbnailCategory')
const twitterCategory = require('../Schema/TwitterCategory')
const twitterBannerCategory = require('../Schema/TwitterBannerCategory')
const logoCategory = require('../Schema/LogoCategory')
const posterCategory = require('../Schema/PosterCategory')
const pinterestCategory = require('../Schema/PinterestGraphic')
const productHuntCategory = require('../Schema/ProductHuntGallery')
const linkedinCarouselCategory = require('../Schema/LinkedInCarouselCategory')

// logger
const logger = require('../logger');


router.get("/best", async (req, res) => {
    try {
        const data = await adminTemplates.find({ tags: 'best' })
        let filteredCategoryData = data.map(data => {
            try {
                let userData = JSON.parse(JSON.parse(JSON.stringify(data.template)))
                return {
                    _id: data._id,
                    image: userData[0].image,
                    description: data.description
                }
            } catch (error) {
                return null
            }
        }).filter(item => item !== null);

        res.send(filteredCategoryData)
    } catch (error) {
        res.status(500).json({
            error
        })
    }
})

router.get("/threads/template", async (req, res) => {
    try {
        const skip = 20 * (parseInt(req.query.page) - 1) || 0

        const query = {
            templateType: 'instagram post vertical',
            tags: "threads post"
        };

        const [count, categoryData] = await Promise.all([
            adminTemplates.countDocuments(query),
            adminTemplates.find(query, { template: 1, projectName: 1, description: 1 })
                .sort({ $natural: -1 })
                .skip(skip)
                .limit(20)
        ]);

        let filteredCategoryData = categoryData.map(data => {
            try {
                let userData = JSON.parse(JSON.parse(JSON.stringify(data.template)))
                return {
                    _id: data._id,
                    image: userData[0].image,
                    projectName: data.projectName,
                    description: data.description
                }
            } catch (error) {
                return null
            }
        }).filter(item => item !== null);

        res.status(200).json({
            data: filteredCategoryData,
            count: count
        })
    } catch (error) {
        res.status(500).json({
            error
        })
    }
})

const getCategory = async (req, isAdmin) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        let categoryData

        if (req.params.type === "landscape") {
            if (isAdmin) {
                categoryData = await landscapeCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await landscapeCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "story") {
            if (isAdmin) {
                categoryData = await storyCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await storyCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "instagram") {
            if (isAdmin) {
                categoryData = await instagramCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await instagramCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "instagram post vertical") {
            if (isAdmin) {
                categoryData = await instagramVerticalCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await instagramVerticalCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "presentation") {
            if (isAdmin) {
                categoryData = await presentationCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await presentationCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "facebook") {
            if (isAdmin) {
                categoryData = await facebookCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await facebookCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "youtube thumbnail") {
            if (isAdmin) {
                categoryData = await youtubeThumbnailCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await youtubeThumbnailCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "twitter") {
            if (isAdmin) {
                categoryData = await twitterCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await twitterCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "twitter banner") {
            if (isAdmin) {
                categoryData = await twitterBannerCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await twitterBannerCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "logo") {
            if (isAdmin) {
                categoryData = await logoCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await logoCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "poster") {
            if (isAdmin) {
                categoryData = await posterCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await posterCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "pinterest graphic") {
            if (isAdmin) {
                categoryData = await pinterestCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await pinterestCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "product hunt gallery") {
            if (isAdmin) {
                categoryData = await productHuntCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await productHuntCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else if (req.params.type === "linkedin carousel") {
            if (isAdmin) {
                categoryData = await linkedinCarouselCategory.find({ posts: { $gt: 0 } }).skip(skip).limit(limit)
            } else {
                categoryData = await linkedinCarouselCategory.find({ posts: { $gt: 0 }, category: { $nin: 'admin' } }).skip(skip).limit(limit)
            }
        } else {
            // invalid type return blank array
            return []
        }

        return categoryData
    } catch (error) {
        logger.error(error)
        return []
    }
}

router.get('/count', async (req, res) => {
    try {
        let count = await adminTemplates.count({})
        res.status(200).json({ data: count })
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get("/edit/:id", checkAuth, async (req, res) => {
    try {
        let data = await adminTemplates.findById(req.params.id)
        if (data === null) return res.status(400).json({ error: "template not found" })
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get("/templates/slideshow", checkAuth, async (req, res) => {
    try {
        const limit = 10
        let templateTypes = [
            {
                "type": "instagram",
                label: 'Instagram Post Square',
            },
            {
                "type": "instagram post vertical",
                label: 'Instagram Post Potrait',
            },
            {
                "type": "story",
                label: 'Story',
            },
            {
                "type": "landscape",
                label: 'Landscape',
            },
            {
                "type": "presentation",
                label: 'Presentation',
            },
            {
                "type": "poster",
                label: 'Poster',
            },
            {
                "type": "business card standard",
                label: 'Business Card Standard',
            },
            {
                "type": "youtube thumbnail",
                label: 'YouTube Thumbnail',
            },
            {
                "type": "twitter banner",
                label: 'Twitter Banner',
            },
            {
                "type": "twitter",
                label: 'Twitter Post',
            },
            {
                "type": "logo",
                label: 'Logo',
            },
            {
                "type": "pinterest graphic",
                label: 'Pinterest Graphic',
            },
            {
                "type": "product hunt gallery",
                label: 'Product Hunt Gallery',
            },
            {
                "type": "instagram ad",
                label: 'Instagram Ad',
            },
            {
                "type": "instagram story ad",
                label: 'Instagram Story Ad',
            },
            {
                "type": "linkedin carousel",
                label: 'Linkedin Carousel',
            },
            {
                "type": "instagram feed ad",
                label: 'Instagram Feed Ad',
            },
            {
                "type": "facebook ad",
                label: 'Facebook Ad',
            },
            {
                "type": "instagram story highlight cover",
                label: 'Instagram Story Highlight Cover',
            },
            {
                "type": "facebook",
                label: 'Facebook Post',
            },
            {
                "type": "instagram reel cover",
                label: 'Instagram Reel Cover',
            },
            {
                "type": "instagram ad vertical",
                label: 'Instagram Ad Potrait',
            },
            {
                "type": "instagram profile picture",
                label: 'Instagram Profile Picture',
            }
        ]

        const templatesData = await Promise.all(templateTypes.map(async (template) => {
            const data = await adminTemplates.find({ templateType: template.type, verified: true, component: false }, { __v: 0 }).sort({ $natural: -1 }).limit(limit).lean()
            return {
                type: template.label,
                realType: template.type,
                data
            }
        }))

        res.status(200).json({ templatesData: templatesData.flat() })
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get("/templates/:type", checkAuth, async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0

        let data
        if (req.body.userData.isAdmin) {
            data = await adminTemplates.find({ templateType: req.params.type, component: false }).sort({ $natural: -1 }).skip(skip).limit(limit)
        } else {
            data = await adminTemplates.find({ templateType: req.params.type, verified: true, component: false }).sort({ $natural: -1 }).skip(skip).limit(limit)
        }

        res.status(200).json({ data })
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get('/type/:type', checkAuth, async (req, res) => {
    try {
        if (req.body.userData.isAdmin === true) {
            // get the category for the perticular type
            const categoryData = await getCategory(req, true)

            let arrayOfData = []
            for await (const data of categoryData) {
                let obj = {
                    category: data.category,
                    data: []
                }
                obj.data = await adminTemplates.find({ templateType: req.params.type, component: false, category: data.category }).sort({ $natural: -1 }).limit(5)

                if (obj.data.length !== 0) {
                    arrayOfData.push(obj)
                }
            }
            res.send(arrayOfData)
        } else {
            const categoryData = await getCategory(req, false)

            let arrayOfData = []
            for await (const data of categoryData) {
                let obj = {
                    category: data.category,
                    data: []
                }
                obj.data = await adminTemplates.find({ templateType: req.params.type, verified: true, component: false, category: data.category }).sort({ $natural: -1 }).limit(5)
                if (obj.data.length !== 0) {
                    arrayOfData.push(obj)
                }
            }

            res.send(arrayOfData)
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})

// component as per category
router.get("/components/categories/:category", checkAuth, async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        let data = await adminTemplates.find({ component: true, category: req.params.category }).sort({ $natural: -1 }).skip(skip).limit(limit)
        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})


// component as per category
router.get("/components/text/search", checkAuth, async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0
        const query = req.query.query.trim().toLowerCase(); // Trim whitespace

        const data = await adminTemplates.find({
            template: { $regex: query, $options: 'i' },
            component: true,
            category: "text"
        }).sort({ $natural: -1 }).skip(skip).limit(limit)

        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})


// all components
router.get('/components', checkAuth, async (req, res) => {
    try {
        if (req.body.userData.isAdmin === true) {
            const categoryData = await componentCategory.find({ posts: { $gt: 0 } }).sort({ rank: -1 })
            let arrayOfData = []
            for await (const data of categoryData) {
                let obj = {
                    category: data.category,
                    data: []
                }
                obj.data = await adminTemplates.find({ component: true, category: data.category }).sort({ $natural: -1 }).limit(5)
                if (obj.data.length !== 0) {
                    arrayOfData.push(obj)
                }
            }
            res.send(arrayOfData)
        } else {
            const categoryData = await componentCategory.find({ posts: { $gt: 0 } }).sort({ rank: -1 })
            let arrayOfData = []
            for await (const data of categoryData) {
                let obj = {
                    category: data.category,
                    data: []
                }
                obj.data = await adminTemplates.find({ verified: true, component: true, category: data.category }).sort({ $natural: -1 }).limit(5)
                if (obj.data.length !== 0) {
                    arrayOfData.push(obj)
                }
            }
            res.send(arrayOfData)
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})

const getTheMediaFromArray = (data) => {
    const imagesArray = []
    const CanvasData = JSON.parse(data.template)
    CanvasData.forEach((objects) => {
        objects.arrayOfElements.forEach(element => {
            if (element.type === 'image') {
                imagesArray.push(element.src)
            } else if (element.type === 'svg') {
                imagesArray.push(element.src)
            } else if (element.type === 'mask') {
                imagesArray.push(element.src)
                imagesArray.push(element.src1)
            } else if (element.type === 'group') {
                element.data.forEach(groupElement => {
                    if (groupElement.type === 'image') {
                        imagesArray.push(groupElement.src)
                    } else if (groupElement.type === 'svg') {
                        imagesArray.push(groupElement.src)
                    } else if (groupElement.type === 'mask') {
                        imagesArray.push(groupElement.src)
                        imagesArray.push(groupElement.src1)
                    }
                })
            }
        })
    })
    return imagesArray
}


const changeTheSrcOfMedia = async (data, newURL, dataBaseData) => {
    try {
        let CanvasData = JSON.parse(data.template)
        newURL.forEach(data => {
            CanvasData.forEach((objects) => {
                objects.arrayOfElements.forEach(element => {
                    if (element.type === 'image') {
                        if (data.oldURL !== element.src) return
                        element.src = data.newURL
                    } else if (element.type === 'svg') {
                        if (data.oldURL !== element.src) return
                        element.src = data.newURL
                    } else if (element.type === 'mask') {
                        if (data.oldURL === element.src) {
                            element.src = data.newURL
                        }
                        if (data.oldURL === element.src1) {
                            element.src1 = data.newURL
                        }
                    } else if (element.type === 'group') {
                        element.data.forEach(groupElement => {
                            if (groupElement.type === 'image') {
                                if (data.oldURL !== groupElement.src) return
                                groupElement.src = data.newURL
                            } else if (groupElement.type === 'svg') {
                                if (data.oldURL !== groupElement.src) return
                                groupElement.src = data.newURL
                            } else if (groupElement.type === 'mask') {
                                if (data.oldURL === groupElement.src) {
                                    groupElement.src = data.newURL
                                }
                                if (data.oldURL === groupElement.src1) {
                                    groupElement.src1 = data.newURL
                                }
                            }
                        })
                    }
                })
            })
        })

        dataBaseData.template = JSON.stringify(CanvasData)
        await dataBaseData.save()
    } catch (error) {
        logger.error(error)
    }
}


const mapTheObjectToDownload = async (data, dataBaseData) => {
    try {
        let media = getTheMediaFromArray(data)
        media = [...new Set(media)];

        let newURL = media.map(async (url) => {
            const mediaIsThere = await templateImages.findOne({ originalURL: url })
            if (mediaIsThere === null) {
                const object = await downloadTheImageFromUrlAndStoreToS3(url, data.userId)
                return object
            } else {
                return {
                    oldURL: mediaIsThere.originalURL,
                    newURL: mediaIsThere.s3URL
                }
            }
        })
        newURL = await Promise.all(newURL)

        changeTheSrcOfMedia(data, newURL, dataBaseData)
    } catch (error) {
        logger.error(error)
    }
}


const createCategory = async (category, req, removeCategoryPost) => {
    try {
        // remove the the count of the post
        for await (const cat of removeCategoryPost) {
            let categoryIsThere = await category.findOne({ category: cat })
            if (categoryIsThere !== null) {
                categoryIsThere.posts = categoryIsThere.posts - 1
                await categoryIsThere.save()
            }
        }

        for await (const cat of req.body.category) {
            let categoryIsThere = await category.findOne({ category: cat })
            if (categoryIsThere === null) {
                let obj = {
                    category: cat,
                    rank: 0,
                    posts: 1,
                }
                await category.create(obj)
            } else {
                categoryIsThere.posts = categoryIsThere.posts + 1
                await categoryIsThere.save()
            }
        }
    } catch (error) {
        logger.error(error)
    }
}

const addAndManageCategory = (req, removeCategoryPost) => {
    if (req.body.component === true) {
        createCategory(componentCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "landscape") {
        createCategory(landscapeCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "story") {
        createCategory(storyCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "instagram") {
        createCategory(instagramCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "instagram post vertical") {
        createCategory(instagramVerticalCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "presentation") {
        createCategory(presentationCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "facebook") {
        createCategory(facebookCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "youtube thumbnail") {
        createCategory(youtubeThumbnailCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "twitter") {
        createCategory(twitterCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "twitter banner") {
        createCategory(twitterBannerCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "logo") {
        createCategory(logoCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "poster") {
        createCategory(posterCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "pinterest graphic") {
        createCategory(pinterestCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "product hunt gallery") {
        createCategory(productHuntCategory, req, removeCategoryPost)
    } else if (req.body.templateType === "linkedin carousel") {
        createCategory(linkedinCarouselCategory, req, removeCategoryPost)
    }
}


// create template
router.post('/', async (req, res) => {
    try {
        if (req.body.category?.trim().length === 0) {
            req.body.category = 'admin'
        }

        // beautify the data
        req.body.category = req.body.category?.split(',').map(data => {
            return data.trim().toLowerCase()
        })

        addAndManageCategory(req, [])

        let CanvasData = JSON.parse(req.body.template)

        // add base64 images to s3 bucket
        let urls = CanvasData.map(async (base64) => {
            let data = await addBase64DataToS3Bucket(base64.image)
            return data
        })
        urls = await Promise.all(urls)

        // add the preview url
        let previewData = CanvasData.map((data, index) => {
            data.image = urls[index]
            return data
        })

        //updated the added url in template
        req.body.template = JSON.stringify(previewData)
        const data = await adminTemplates.create(req.body)

        res.status(201).json({
            message: "done",
            id: data._id
        })

        mapTheObjectToDownload(req.body, data)
    } catch (error) {
        console.log(error)
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})


//muse update
router.post('/update', async (req, res) => {
    try {
        const data = await adminTemplates.findById(req.body.id)

        // delete old files
        let deleteFilePromise = JSON.parse(data.template).map(async (elements) => {
            let data = await deleteOldFile(elements.image?.split('/').pop())
            return data
        })

        deleteFilePromise = await Promise.all(deleteFilePromise)

        if (req.body.category?.trim().length === 0) {
            req.body.category = 'admin'
        }

        // beautify the data
        req.body.category = req.body.category?.split(',').map(data => {
            return data.trim().toLowerCase()
        })

        // find the category which is not in the req body
        let categoryToSubtractCount = data.category.filter(category => !req.body.category.includes(category))

        let dataToStoreCategory = req.body.category
        req.body.category = req.body.category.filter(category => !data.category.includes(category))

        addAndManageCategory(req, categoryToSubtractCount)

        let canvasData = JSON.parse(req.body.template)

        // add base64 images to s3 bucket
        let urls = canvasData.map(async (base64) => {
            let data = await addBase64DataToS3Bucket(base64.image)
            return data
        })

        urls = await Promise.all(urls)

        // add the preview url
        let previewData = canvasData.map((data, index) => {
            data.image = urls[index]
            return data
        })

        //updated the added url in template
        req.body.template = JSON.stringify(previewData)

        data.canvasMultiply = req.body.canvasMultiply
        data.projectName = req.body.projectName
        data.description = req.body.description
        data.colors = req.body.colors
        data.category = dataToStoreCategory
        data.tags = req.body.tags
        data.region = req.body.region
        data.templateType = req.body.templateType
        data.aspectRatio = req.body.aspectRatio
        data.language = req.body.language
        data.categoryDate = req.body.categoryDate
        data.prime = req.body.prime
        data.template = req.body.template
        data.verified = req.body.verified

        const newData = await data.save()

        res.status(201).send("done")
        mapTheObjectToDownload(req.body, newData)
    } catch (error) {
        console.log(error)
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})

router.put('/', async (req, res) => {
    try {
        if (req.body.category.length === 0) {
            req.body.category = ['admin'];
        }

        let data = await adminTemplates.findById(req.body._id);

        // find the category which is not in the req body
        let categoryToSubtractCount = data.category.filter(category => !req.body.category.includes(category))

        let dataToStoreCategory = req.body.category
        req.body.category = req.body.category.filter(category => !data.category.includes(category))

        if (data.component) {
            req.body.component = true
            req.body.templateType = ""
            addAndManageCategory(req, categoryToSubtractCount)
        } else {
            addAndManageCategory(req, categoryToSubtractCount)
        }

        data.projectName = req.body.projectName;
        data.description = req.body.description;
        data.colors = req.body.colors;
        data.category = dataToStoreCategory;
        data.tags = req.body.tags;
        data.region = req.body.region;
        data.templateType = req.body.templateType;
        data.aspectRatio = req.body.aspectRatio;
        data.language = req.body.language;
        data.categoryDate = req.body.categoryDate;
        data.prime = req.body.prime;
        data.verified = req.body.verified;
        data = await data.save();
        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error,
        });
    }
});

// delete template
router.delete('/delete/template/:templateId', async (req, res) => {
    try {
        const { templateId } = req.params;
        const template = await adminTemplates.findOne({ _id: templateId })

        if (template === null) return res.status(400).json({ error: 'not found' })
        req.body.category = []

        let categoryToSubtractCount = template.category

        if (template.component) {
            req.body.component = true
            req.body.templateType = ""
            addAndManageCategory(req, categoryToSubtractCount)
        } else {
            req.body.templateType = template.templateType
            addAndManageCategory(req, categoryToSubtractCount)
        }

        await adminTemplates.findByIdAndDelete({ _id: templateId });
        return res.status(200).json({
            message: 'Template deleted successfully',
        });
    } catch (error) {
        logger.error(error)
        return res.status(500).json({
            error: 'Server Error',
        });
    }
})

// @desc search templates by category
router.get("/categories/:type/:category", checkAuth, async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0

        // show all templates to user of following template type
        let arrayOfTemplateTypes = ['logo', 'poster', "twitter banner", "twitter", 'presentation', 'facebook', 'youtube thumbnail']

        // show all templates to user of perticular template type without category
        if (arrayOfTemplateTypes.includes(req.params.type)) {
            if (req.body.userData.isAdmin === true) {
                data = await adminTemplates.find({ templateType: req.params.type, component: false }).sort({ votes: 1, _id: -1 }).skip(skip).limit(limit)
                return res.send(data)
            } else {
                data = await adminTemplates.find({ templateType: req.params.type, component: false, verified: true }).sort({ votes: 1, _id: -1 }).skip(skip).limit(limit)
                return res.send(data)
            }
        }


        if (req.body.userData.isAdmin === true) {
            let data
            if (req.params.category === "admin") {
                req.params.category = ""
                data = await adminTemplates.find({ $or: [{ category: 'admin' }, { category: "" }], templateType: req.params.type, component: false }).sort({ votes: 1, _id: -1 }).skip(skip).limit(limit)
            } else {
                data = await adminTemplates.find({ category: req.params.category, templateType: req.params.type, component: false }).sort({ votes: 1, _id: -1 }).skip(skip).limit(limit)
            }
            res.send(data)
        } else {
            const data = await adminTemplates.find({ category: req.params.category, templateType: req.params.type, component: false, verified: true }).sort({ votes: 1, _id: -1 }).skip(skip).limit(limit)
            res.send(data)
        }
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})


// @ADMIN PANEL ROUTE
// @Route muse/admintemplates/all
// @desc search the templates from data base for admin panel as per filter with pagination
router.get('/all', async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 10
        const skip = limit * (parseInt(req.query.page) - 1) || 0

        let data

        if (req.query.filter === "components") {
            data = await adminTemplates.find({ component: true }).sort({ $natural: -1 }).skip(skip).limit(limit)
        } else if (req.query.filter === "all") {
            data = await adminTemplates.find({}).sort({ $natural: -1 }).skip(skip).limit(limit)
        } else if (req.query.filter === "incomplete") {
            data = await adminTemplates.find({
                $or: [
                    { projectName: "" },
                    { description: "" },
                    { $expr: { $eq: [{ $size: "$colors" }, 0] } },
                    { $expr: { $eq: [{ $size: "$category" }, 0] } },
                    { $expr: { $eq: [{ $size: "$tags" }, 0] } },
                ]
            }).sort({ $natural: -1 }).skip(skip).limit(limit)
        } else if (req.query.filter === "complete") {
            data = await adminTemplates.find({
                $and: [
                    { projectName: { $ne: "" } },
                    { description: { $ne: "" } },
                    { $expr: { $ne: [{ $size: "$colors" }, 0] } },
                    { $expr: { $ne: [{ $size: "$category" }, 0] } },
                    { $expr: { $ne: [{ $size: "$tags" }, 0] } },
                ]
            }).sort({ $natural: 1 }).skip(skip).limit(limit);
        } else {
            data = await adminTemplates.find({ templateType: req.query.filter, component: false }).sort({ $natural: -1 }).skip(skip).limit(limit)
        }

        res.send(data)
    } catch (error) {
        logger.error(error)
        res.status(500).json({
            error: error
        })
    }
})

// @ADMIN PANEL ROUTE
// @Route muse/admintemplates/templates
// @desc search the templates from data base for admin panel
router.get("/templates", async (req, res) => {
    try {
        let { query, page, per_page } = req.query;
        if (!page) {
            page = 1;
        }

        if (!per_page) {
            per_page = 50;
        }
        const pagenum = parseInt(page);
        const limit = parseInt(per_page);
        const skipIndex = (pagenum - 1) * limit;
        const data = await adminTemplates.find({ $text: { $search: query } })
            .collation({
                locale: 'en',
                strength: 2,
            })
            .sort({ votes: 1, _id: -1 })
            .limit(limit)
            .skip(skipIndex);
        return res.status(200).json(data);
    } catch (error) {
        logger.error(error)
        return res.status(500).json({
            error: 'Server Error',
        });
    }
})


module.exports = router