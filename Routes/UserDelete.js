const express = require('express')
const router = express.Router()
const { deleteOldFile } = require('../Functions/S3')
const CheckAuth = require('../Middleware/CheckAuth')

const userUploads = require('../Schema/UserUploadsSchema')
const template = require('../Schema/TemplateSchema')

router.delete("/", CheckAuth, async (req, res) => {
    try {
        const { id } = req.body.userData
        const images = await userUploads.find({ userId: id })

        await template.deleteMany({ userId: id })
        const imagesPromise = images.map(async (data) => {
            return await deleteOldFile(data.fileName)
        })

        await Promise.all(imagesPromise)
        res.send("done")
    } catch (error) {
        res.status(500).json({ error: "Internal server error" })
    }
})


module.exports = router