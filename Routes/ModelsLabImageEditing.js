const express = require("express")
const router = express.Router()
const axios = require("axios")
const MODELSLAB_API_TOKEN = 'C0CMtV0PuI4PnjXBxqHGfZa6b0R9d60vAd5Km1gYOmP4NVa3V5OIAqgaPaIn'
const REPLICATE_API_TOKEN = 'r8_8ZOGAsySTe4ttT2lFRdPDhkeL2593CY0XuunC'
const REPLICATE_URL = 'https://api.replicate.com/v1/predictions'

const checkImageStatusModelsLab = async (predictionId, callCount = 0) => {
    try {
        if (callCount >= 20) {
            throw new Error('Exceeded maximum number of calls');
        }

        const statusResponse = await axios.post("https://modelslab.com/api/v6/image_editing/fetch/" + predictionId, {
            "key": MODELSLAB_API_TOKEN,
        })

        if (statusResponse.data.status === "failed") {
            throw new Error('Failed to generate image');
        } else if (statusResponse.data.status !== "success") {
            await new Promise(resolve => setTimeout(resolve, 5000));
            return checkImageStatusModelsLab(predictionId, callCount + 1);
        } else {
            return statusResponse.data;
        }
    } catch (error) {
        console.log(error)
        throw new Error('Failed to fetch data');
    }
}


const checkImageStatusOfReplicate = async (predictionId, callCount = 0) => {
    try {
        if (callCount >= 20) {
            throw new Error('Exceeded maximum number of calls');
        }

        const statusUrl = `${REPLICATE_URL}/${predictionId}`;
        const statusHeaders = {
            'Authorization': `Token ${REPLICATE_API_TOKEN}`
        };

        const statusResponse = await axios.get(statusUrl, { headers: statusHeaders });

        if (statusResponse.data.status === "failed") {
            throw new Error('Failed to generate image');
        } else if (statusResponse.data.status !== "succeeded") {
            await new Promise(resolve => setTimeout(resolve, 5000));
            return checkImageStatusOfReplicate(predictionId, callCount + 1);
        } else {
            return statusResponse.data;
        }
    } catch (error) {
        console.log(error)
        throw new Error('Failed to fetch data');
    }
}

router.post("/remove-background", async (req, res) => {
    try {
        const response = await axios.post("https://modelslab.com/api/v6/image_editing/removebg_mask", {
            key: MODELSLAB_API_TOKEN,
            image: req.body.url,
            seed: null,
            post_process_mask: false,
            alpha_matting: true,
            only_mask: false,
            alpha_matting_erode_size: 0
        })

        if (response.data.status === "success") {
            res.send(response.data.output)
        } else {
            // wait till the estimated time
            await new Promise(resolve => setTimeout(resolve, parseInt(response.data.eta) * 1000));

            // Get final image status
            const finalImageData = await checkImageStatusModelsLab(response.data.id);
            res.send(finalImageData.output)
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error.message || "Internal Server Error"
        });
    }
})

router.post("/increase-resolution", async (req, res) => {
    try {
        const response = await axios.post("https://modelslab.com/api/v6/image_editing/super_resolution", {
            key: MODELSLAB_API_TOKEN,
            url: req.body.url,
            scale: 2,
            face_enhance: false,
            model_id: 'RealESRGAN_x4plus'
        })

        if (response.data.status === "success") {
            res.send(response.data.output)
        } else {
            // wait till the estimated time
            await new Promise(resolve => setTimeout(resolve, parseInt(response.data.eta) * 1000));

            // Get final image status
            const finalImageData = await checkImageStatusModelsLab(response.data.id);
            res.send(finalImageData.output)
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error.message || "Internal Server Error"
        });
    }
})

router.post("/reimagine", async (req, res) => {
    try {
        const response = await axios.post("https://modelslab.com/api/v6/realtime/img2img", {
            key: MODELSLAB_API_TOKEN,
            prompt: "give different version of the same image",
            negative_prompt: "bad image",
            init_image: req.body.url,
            width: 1024,
            height: 1024,
            samples: 3,
            safety_checker: true,
            strength: 0.7,
            temp:true
        })


        console.log(response.data)
        if (response.data.status === "success") {
            res.send(response.data.output)
        } else {
            // wait till the estimated time
            await new Promise(resolve => setTimeout(resolve, parseInt(response.data.eta) * 1000));

            // Get final image status
            const finalImageData = await checkImageStatusModelsLab(response.data.id);
            res.send(finalImageData.output)
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error.message || "Internal Server Error"
        });
    }
})


router.post("/out-painting", async (req, res) => {
    try {
        const response = await axios.post(REPLICATE_URL, {
            version: "fd4c82414feb3f592853b736da996d1a949b37ddc1b969f1afbaeadfa1b7ef83",
            input: {
                image: req.body.url,
                prompt: "",
                scheduler: "K_EULER",
                lora_scale: 0.8,
                num_outputs: 1,
                outpaint_size: 512,
                guidance_scale: 7.5,
                apply_watermark: false,
                condition_scale: 0.25,
                negative_prompt: "",
                outpaint_direction: "left"
            }
        }, {
            headers: {
                Authorization: `Token ${REPLICATE_API_TOKEN}`,
                'Content-Type': 'application/json'
            }
        })

        if (response.data.status === "success") {
            res.send(response.data.output)
        } else {
            // Get final image status
            const finalImageData = await checkImageStatusOfReplicate(response.data.id);
            res.send(finalImageData.output)
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error.message || "Internal Server Error"
        });
    }
})


router.post("/in-painting", async (req, res) => {
    try {
        const response = await axios.post("https://modelslab.com/api/v6/image_editing/inpaint", {
            key: MODELSLAB_API_TOKEN,
            prompt: "add a cat face",
            negative_prompt: null,
            init_image: "https://d1vxvcbndiq6tb.cloudfront.net/fit-in/1024x0/db4bc5d7-895b-41ab-866a-7e2fa8823980",
            mask_image: "https://d1vxvcbndiq6tb.cloudfront.net/fit-in/1024x0/0397ad9a-410e-44d2-b011-7463f6ad7493",
            width: req.body.width,
            height: req.body.height,
            samples: 3,
            num_inference_steps: "30",
            safety_checker: false,
            enhance_prompt: "yes",
            guidance_scale: 5,
            strength: 0.7,
            base64: false,
            seed: null,
            webhook: null,
        })

        if (response.data.status === "success") {
            res.send(response.data.output)
        } else {
            // wait till the estimated time
            await new Promise(resolve => setTimeout(resolve, parseInt(response.data.eta) * 1000));

            // Get final image status
            const finalImageData = await checkImageStatusModelsLab(response.data.id);
            res.send(finalImageData.output)
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error.message || "Internal Server Error"
        });
    }
})

module.exports = router;