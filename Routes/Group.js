const express = require("express")
const router = express.Router()
const Group = require("../Schema/Group")
const mongoose = require('mongoose');

router.get("/", async (req, res) => {
    try {
        const groupData = await Group.find({}, { __v: 0 }).sort({ rank: 1 })
        res.send(groupData)
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: "Internal server error" })
    }
})

router.post("/", async (req, res) => {
    try {
        if (!req.body.name || !req.body.tags) {
            return res.status(400).json({ error: "Name and tags are required." });
        }

        let groupCount = await Group.countDocuments()

        const obj = {
            name: req.body.name,
            tags: req.body.tags,
            rank: groupCount + 1,
            templateType: req.body.templateType,
            templateIds: req.body.templateIds || []
        }

        const createdGroup = await Group.create(obj)
        res.status(201).json(createdGroup);
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: "Internal server error" })
    }
})

router.put("/", async (req, res) => {
    try {
        const groupData = await Group.findById(req.body.id)
        groupData.name = req.body.name
        groupData.tags = req.body.tags
        groupData.templateType = req.body.templateType
        groupData.templateIds = req.body.templateIds

        const updatedGroupData = await groupData.save()
        res.send(updatedGroupData)
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: "Internal server error" })
    }
})

router.put("/rank", async (req, res) => {
    try {
        let dataToUpdate = req.body.groups

        const updateOperations = dataToUpdate.map(update => ({
            updateOne: {
                filter: { _id: mongoose.Types.ObjectId(update.id) },
                update: { $set: { rank: update.rank } }
            }
        }));

        await Group.bulkWrite(updateOperations)
        res.send("Updated")
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: "Internal server error" })
    }
})

module.exports = router